import React, { useState } from 'react'
import { ScrollView, TouchableOpacity } from 'react-native'
import Text from '../../components/text'
import Header from '../../components/header'

import Model5 from '../../assets/images/modelo_5x.png'
import Model6 from '../../assets/images/modelo_6x.png'
import Model7 from '../../assets/images/modelo_7x.png'
import Model8 from '../../assets/images/modelo_8x.png'

import { Ionicons, MaterialCommunityIcons } from '@expo/vector-icons'

import ProductDetails from '../../components/productDetails'
import {
  ContentCategoryItems,
  WrapperFilterItems,
  OrganizationContent
} from './styles'

const CategoryItems: React.FC = () => {
  const [organization, setOrganization] = useState('50%')

  const productData = [
    {
      text: 'Camisa de seda parka mi…',
      image: Model5,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80',
      sale: {
        oldValue: 'R$ 898',
        newValue: 'R$ 529'
      }
    },
    {
      text: 'Camisa de seda parka mi…',
      image: Model7,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80'
    },
    {
      text: 'Camisa de seda parka mi…',
      image: Model6,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80'
    },
    {
      text: 'Camisa de seda parka mi…',
      image: Model8,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80'
    }
  ]

  return (
    <ScrollView style={{ backgroundColor: '#fff' }}>
      <Header pageTitle="Vestido" />
      <WrapperFilterItems>
        <Text
          text={`${productData.length} produtos`}
          textColor="#969696"
          textSize={18}
          texTransform="none"
          textFont="jost-light"
        />
        <OrganizationContent>
          <TouchableOpacity onPress={() => setOrganization('50%')}>
            <Ionicons
              name="grid-sharp"
              size={24}
              color={organization === '50%' ? '#000' : '#F3F3F3'}
            />
          </TouchableOpacity>

          <TouchableOpacity onPress={() => setOrganization('100%')}>
            <MaterialCommunityIcons
              name="square"
              size={28}
              color={organization === '100%' ? '#000' : '#F3F3F3'}
            />
          </TouchableOpacity>
        </OrganizationContent>
      </WrapperFilterItems>
      <ContentCategoryItems>
        {productData.map((item, index) => {
          return (
            <ProductDetails
              internalSpace={true}
              key={`category_item_${index}`}
              width={organization}
              {...item}
            />
          )
        })}
      </ContentCategoryItems>
    </ScrollView>
  )
}

export default CategoryItems
