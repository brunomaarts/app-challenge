import React from 'react'
import { View, Text } from 'react-native'

const Conta: React.FC = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>Conta!</Text>
    </View>
  )
}

export default Conta
