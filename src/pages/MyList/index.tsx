import React from 'react'
import { View, Text } from 'react-native'

const MyList: React.FC = () => {
  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Text>MyList!</Text>
    </View>
  )
}

export default MyList
