import React from 'react'
import { useNavigation } from '@react-navigation/native'
import { ScrollView } from 'react-native'

import ActionBox from '../../components/actionBox'
import NewIn from '../../components/newIn'
import Section from '../../components/section'

import Model3 from '../../assets/images/modelo_3x.png'
import Model4 from '../../assets/images/modelo_4x.png'
import Model5 from '../../assets/images/modelo_5x.png'
import Model6 from '../../assets/images/modelo_6x.png'
import Model7 from '../../assets/images/modelo_7x.png'
import Model8 from '../../assets/images/modelo_8x.png'

import Carousel from '../../components/carousel'
import Product from '../../components/product'
import FooterLink from '../../components/footerLink'

const Home: React.FC = () => {
  const carouselData = [
    {
      url: Model4,
      id: 1
    },
    {
      url: Model4,
      id: 1
    },
    {
      url: Model4,
      id: 1
    }
  ]

  const productData = [
    {
      text: 'Camisa de seda parka mi…',
      image: Model5,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80'
    },
    {
      text: 'Camisa de seda parka mi…',
      image: Model7,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80'
    },
    {
      text: 'Camisa de seda parka mi…',
      image: Model6,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80'
    },
    {
      text: 'Camisa de seda parka mi…',
      image: Model8,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80'
    }
  ]

  const productLikeData = [
    {
      text: 'Camisa de seda parka mi…',
      image: Model6,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80'
    },
    {
      text: 'Camisa de seda parka mi…',
      image: Model8,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80'
    },
    {
      text: 'Camisa de seda parka mi…',
      image: Model5,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80'
    },
    {
      text: 'Camisa de seda parka mi…',
      image: Model7,
      value: 'R$ 529',
      installmentAmount: '5x de R$ 105,80'
    }
  ]

  return (
    <ScrollView>
      <Carousel data={carouselData} />
      <Section sectionTitle="New in" action={() => false} actionText="Ver tudo">
        <NewIn />
      </Section>

      <Section internalSpace={true}>
        <ActionBox
          btnBgColor="#fff"
          btnType="leaked"
          btnAction={() => false}
          backgroundImage={Model3}
          boxWidth="100%"
          boxHeight="500px"
          titleSize={20}
          titleMargin="30px 0px"
          text="Shop now"
          textColor="#fff"
          textSize={18}
          textAlign="center"
        />
      </Section>

      <Section
        sectionTitle="Vistos recentemente"
        action={() => false}
        actionText="Limpar"
      >
        <Product dataList={productData} />
      </Section>

      <Section sectionTitle="Você pode curtir">
        <Product dataList={productLikeData} />
      </Section>

      <FooterLink />
    </ScrollView>
  )
}

export default Home
