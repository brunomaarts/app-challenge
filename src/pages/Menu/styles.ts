import styled from 'styled-components'
import Constants from 'expo-constants'

export const WrapperMenu = styled.View`
  width: 100%;
  padding-top: ${Constants.statusBarHeight + 20 + 'px'};
`
