import React from 'react'
import { ScrollView } from 'react-native'
import Categories from '../../components/categories'
import Featured from '../../components/featured'
import { WrapperMenu } from './styles'

const Menu: React.FC = () => {
  return (
    <WrapperMenu>
      <ScrollView>
        <Categories />
        <Featured />
      </ScrollView>
    </WrapperMenu>
  )
}

export default Menu
