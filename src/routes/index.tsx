import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { Feather } from '@expo/vector-icons'

import Home from '../pages/Home'
import Menu from '../pages/Menu'
import MyList from '../pages/MyList'
import Conta from '../pages/Conta'

const Routes: React.FC = () => {
  const Tab = createBottomTabNavigator()

  const routes = [
    {
      name: 'Home',
      icon: 'home',
      label: 'início',
      component: Home
    },
    {
      name: 'Menu',
      icon: 'menu',
      label: 'menu',
      component: Menu
    },
    {
      name: 'List',
      icon: 'list',
      label: 'my list',
      component: MyList
    },
    {
      name: 'Account',
      icon: 'user',
      label: 'conta',
      component: Conta
    }
  ]

  return (
    <Tab.Navigator
      tabBarOptions={{
        activeTintColor: '#000000',
        inactiveTintColor: 'rgba(0,0,0,0.4)',
        tabStyle: {
          paddingTop: 30
        },
        labelStyle: {
          marginTop: 12,
          fontSize: 14,
          fontFamily: 'jost-light',
          height: 17
        }
      }}
    >
      {routes.map((item, index) => {
        return (
          <Tab.Screen
            key={`route_${index}`}
            name={item.name}
            component={item.component}
            options={{
              tabBarLabel: item.label,
              tabBarIcon: ({ color }) => (
                <Feather
                  name={item.icon}
                  color={color}
                  size={25}
                  style={{
                    height: 25
                  }}
                />
              )
            }}
          />
        )
      })}
    </Tab.Navigator>
  )
}

export default Routes
