import styled from 'styled-components'
import Constants from 'expo-constants'

export const HeaderPages = styled.View`
  padding: ${Constants.statusBarHeight + 20 + 'px'} 24px 15px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`
