import React from 'react'
import { HeaderPages } from './styles'

import { IHeaderPage } from '../../@types/IHeaderPage'
import Text from '../text'
import { FontAwesome, SimpleLineIcons } from '@expo/vector-icons'
import { TouchableOpacity } from 'react-native-gesture-handler'
import { useNavigation } from '@react-navigation/native'

const Header: React.FC<IHeaderPage> = props => {
  const navigation = useNavigation()
  return (
    <HeaderPages>
      <TouchableOpacity onPress={() => navigation.navigate('Menu')}>
        <FontAwesome name="angle-left" size={30} color="black" />
      </TouchableOpacity>
      <Text text={props.pageTitle} textSize={18} textColor="#000" />
      <TouchableOpacity onPress={() => navigation.navigate('Home')}>
        <SimpleLineIcons name="handbag" size={25} color="black" />
      </TouchableOpacity>
    </HeaderPages>
  )
}

export default Header
