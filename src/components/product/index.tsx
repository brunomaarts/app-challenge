import React from 'react'
import { ContentProduct } from './styles'
import { StyledNewInList } from '../newIn/styles'
import ProductDetails from '../productDetails'

interface IProduct {
  dataList: {
    text: string
    image: string
    value: string
    installmentAmount: string
  }[]
}

const Product: React.FC<IProduct> = props => {
  return (
    <ContentProduct>
      <StyledNewInList
        horizontal={true}
        data={props.dataList}
        keyExtractor={(_, index) => 'product_' + index}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item, index }) => {
          const marginBox =
            index + 1 === props.dataList.length ? '48px' : '20px'
          return <ProductDetails {...item} margin={marginBox} />
        }}
      />
    </ContentProduct>
  )
}

export default Product
