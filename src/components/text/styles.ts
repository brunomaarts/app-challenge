import styled from 'styled-components'
import { IText } from '../../@types/iText'

export const StyledText = styled.Text<IText>`
  font-family: ${props => props.textFont || 'jost-medium'};
  color: ${props => props.textColor || '#000'};
  text-align: ${props => props.textAlign || 'left'};
  text-transform: ${props => props.texTransform || 'uppercase'};
  font-size: ${props => `${props.textSize || 16}px`};
  margin: ${props => `${props.textMargin || '0px'}`};
  text-decoration: ${props => `${props.textDecoration || 'none'}`};
  text-decoration-color: ${props => props.textColor || '#000'};
  z-index: 1;
`
