
import React from 'react'

import { StyledText } from './styles'
import { IText } from '../../@types/IText';

type Props = IText

const Text: React.FC<Props> = props => {
  return <StyledText {...props}>{props?.text}</StyledText>
}

export default Text
