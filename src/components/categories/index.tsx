import React from 'react'

import {
  StyledCategories,
  StyleCategoryItem,
  StyleCategoryItemImage
} from './styles'

import Category_1 from '../../assets/images/category_1x.png'
import Category_2 from '../../assets/images/category_2x.png'
import Category_3 from '../../assets/images/category_3x.png'
import Category_4 from '../../assets/images/category_4x.png'
import Category_5 from '../../assets/images/category_5x.png'

import Text from '../text'
import { useNavigation } from '@react-navigation/native'

const dataList = [
  {
    categoryName: 'Malha',
    categoryImage: Category_5
  },
  {
    categoryName: 'Jaquetas',
    categoryImage: Category_1
  },
  {
    categoryName: 'Tricot',
    categoryImage: Category_2
  },
  {
    categoryName: 'Blusas',
    categoryImage: Category_3
  },
  {
    categoryName: 'Shop',
    categoryImage: Category_4
  }
]

const Categories: React.FC = () => {
  const navigation = useNavigation()
  return (
    <StyledCategories
      horizontal={true}
      data={dataList}
      keyExtractor={(item, index) => 'newin_' + index}
      showsHorizontalScrollIndicator={false}
      renderItem={({ item, index }) => (
        <StyleCategoryItem onPress={() => navigation.navigate('CategoryItems')}>
          <StyleCategoryItemImage source={item.categoryImage} />
          <Text
            text={item.categoryName}
            textSize={16}
            textColor="#000"
            texTransform="none"
            textAlign="center"
            textFont="jost-light"
            textMargin="5px 0px 0px"
          />
        </StyleCategoryItem>
      )}
    />
  )
}

export default Categories
