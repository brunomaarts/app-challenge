import styled from 'styled-components'

export const StyledCategories = styled.FlatList`
  padding-left: 10px;
`

export const StyleCategoryItem = styled.TouchableOpacity`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-right: 15px;
`

export const StyleCategoryItemImage = styled.ImageBackground`
  width: 75px;
  height: 75px;
`
