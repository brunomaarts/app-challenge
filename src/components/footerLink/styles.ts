import styled from 'styled-components'

interface IStyledFooterLinkList {
  border: string
}

export const StyledFooterLinkList = styled.View`
  width: 100%;
  margin-top: 30px;
  padding: 0px 24px;
  border-top-width: 1px;
  border-top-color: #d8d8d8;
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
`

export const FooterItem = styled.View<IStyledFooterLinkList>`
  width: 100%;
  border-bottom-width: ${props => props.border + 'px'};
  border-bottom-color: #d8d8d8;
  padding: 24px 0px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`
