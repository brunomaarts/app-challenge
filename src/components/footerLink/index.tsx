import React from 'react'
import { StyledFooterLinkList, FooterItem } from './styles'
import { FontAwesome } from '@expo/vector-icons'
import Text from '../text'

const dataList = [
  {
    icon: '',
    text: 'Encontre uma loja',
    action: () => {
      return false
    }
  },
  {
    icon: '',
    text: 'Fale com um vendedor',
    action: () => {
      return false
    }
  }
]

const FooterLink: React.FC = props => {
  return (
    <StyledFooterLinkList>
      {dataList.map((item, index) => {
        return (
          <FooterItem
            key={`footer_item_${index}`}
            border={index + 1 === dataList.length ? '0' : '1'}
          >
            <Text
              text={item.text}
              textSize={16}
              texTransform="none"
              textColor="#000"
            />
            <FontAwesome name="angle-right" size={18} color="#626262" />
          </FooterItem>
        )
      })}
    </StyledFooterLinkList>
  )
}

export default FooterLink
