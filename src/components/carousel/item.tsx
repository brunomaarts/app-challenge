import React from 'react'
import { Dimensions } from 'react-native'
import { CardView, CardViewImage, CardViewText } from './styles'
import Button from '../button'

const { width, height } = Dimensions.get('window')

interface ICarouselItem {
  title: string
  url?: string
  description?: string
}

interface ICarousel {
  item: ICarouselItem
}

const CarouselItem: React.FC<ICarousel> = props => {
  return (
    <CardView width={width} height={height}>
      <CardViewImage
        width={width}
        height={height}
        source={props.item.url}
        resizeMode="cover"
      />
      <CardViewText width={width}>
        <Button
          text="shop now"
          textColor="#fff"
          textSize={22}
          btnBgColor="#fff"
          textAlign="center"
          btnType="leaked"
          btnAction={() => false}
        ></Button>
      </CardViewText>
    </CardView>
  )
}

export default CarouselItem
