import styled from 'styled-components'

interface StyledCarousel {
  width: string
  height: string
}

export const CardView = styled.View<StyledCarousel>`
  display: flex;
  width: ${props => props.width + 'px'};
  height: ${props => props.height / 1.5 + 'px'};
  background-color: #ffffff;
`

export const CardViewImage = styled.ImageBackground<StyledCarousel>`
  width: 100%;
  height: 100%;
  justify-content: center;
`

export const CardViewText = styled.View<StyledCarousel>`
  position: absolute;
  width: ${props => props.width + 'px'};
  bottom: 60px;
  padding: 0px 80px;
`

export const DotView = styled.View`
  flex-direction: row;
  justify-content: center;
  position: absolute;
  bottom: 10px;
  width: 100%;
`
