import React, { useState } from 'react'
import { View, Dimensions, FlatList, Animated } from 'react-native'
import CarouselItem from './item'
import { DotView } from './styles'

const { width } = Dimensions.get('window')
let flatList

interface ICarousel {
  data: any
}

function infiniteScroll(dataList) {
  const numberOfData = dataList.length
  let scrollValue = 0,
    scrolled = 0

  setInterval(function () {
    scrolled++
    if (scrolled < numberOfData) scrollValue = scrollValue + width
    else {
      scrollValue = 0
      scrolled = 0
    }

    flatList.scrollToOffset({ animated: true, offset: scrollValue })
  }, 3000)
}

const Carousel: React.FC<ICarousel> = props => {
  const scrollX = new Animated.Value(0)
  let position = Animated.divide(scrollX, width)
  const [dataList, setDataList] = useState(props.data)

  // useEffect(() => {
  //   setDataList(props.data)
  //   infiniteScroll(dataList)
  // })

  if (props.data && props.data) {
    return (
      <View>
        <FlatList
          data={props.data}
          ref={flatList => {
            flatList = flatList
          }}
          keyExtractor={(item, index) => 'carousel' + index}
          horizontal
          pagingEnabled
          scrollEnabled
          snapToAlignment="center"
          scrollEventThrottle={16}
          decelerationRate={'fast'}
          showsHorizontalScrollIndicator={false}
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: scrollX } } }],
            { useNativeDriver: false }
          )}
          renderItem={({ item }) => {
            return <CarouselItem item={item} />
          }}
        />
        <DotView>
          {props.data.map((_, i) => {
            let opacity = position.interpolate({
              inputRange: [i - 1, i, i + 1],
              outputRange: [0.3, 1, 0.3],
              extrapolate: 'clamp'
            })
            return (
              <Animated.View
                key={i}
                style={{
                  opacity,
                  height: 2,
                  width: 50,
                  backgroundColor: '#ffffff',
                  margin: 8
                }}
              />
            )
          })}
        </DotView>
      </View>
    )
  }
}

export default Carousel
