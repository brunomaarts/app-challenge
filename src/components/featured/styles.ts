import styled from 'styled-components'

interface IContentFeatured {
  backgroudColor: string
  contentHeight: number
}

interface IFeaturedImage {
  imageWidth?: number
}

interface IFeaturedOrientation {
  orientation?: 'left' | 'right'
}

export const WrapperFeatured = styled.View`
  width: 100%;
  margin-top: 20px;
  margin-bottom: 10px;
  padding-left: 10px;
  padding-right: 10px;
`

export const ContentFeatured = styled.TouchableOpacity<IContentFeatured>`
  background-color: ${props => props.backgroudColor || '#c1c3c2'};
  display: flex;
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
  height: ${props =>
    props.contentHeight ? props.contentHeight + 'px' : '150px'};
  width: 100%;
  margin-bottom: 10px;
`

export const FeaturedBoxText = styled.View<IFeaturedOrientation>`
  position: absolute;
  background-color: transparent;
  z-index: 1;
  padding-left: 24px;
  padding-right: 24px;
  display: flex;
  justify-content: ${props =>
    props.orientation === 'right' ? 'flex-start' : 'flex-end'};
  flex-direction: row;
  width: 100%;
`

export const FeaturedImage = styled.ImageBackground<
  IFeaturedImage & IFeaturedOrientation
>`
  width: ${props => (props.imageWidth ? props.imageWidth + 'px' : '100% ')};
  height: 100%;
  margin-left: ${props => (props.orientation === 'left' ? 0 : 'auto')};
  margin-right: ${props => (props.orientation === 'right' ? 0 : 'auto')};
`
