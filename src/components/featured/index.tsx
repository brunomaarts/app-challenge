import React from 'react'

import {
  ContentFeatured,
  FeaturedBoxText,
  FeaturedImage,
  WrapperFeatured
} from './styles'

import Text from '../text'

import featured_11 from '../../assets/images/featured_11x.png'
import featured_12 from '../../assets/images/featured_12x.png'
import featured_13 from '../../assets/images/featured_13x.png'
import featured_14 from '../../assets/images/featured_14x.png'
import featured_18 from '../../assets/images/featured_18x.png'
import featured_16 from '../../assets/images/featured_16x.png'
import featured_17 from '../../assets/images/featured_17x.png'
import { useNavigation } from '@react-navigation/native'

const dataList = [
  {
    featuredName: 'Coleção',
    featuredImage: featured_11,
    featuredOrientationImage: 'right',
    featuredImageWidth: 280
  },
  {
    featuredName: 'Novidades',
    featuredImage: featured_12,
    featuredOrientationImage: 'right',
    featuredImageWidth: 190
  },
  {
    featuredName: 'Acessórios',
    featuredImage: featured_13,
    featuredOrientationImage: 'right',
    featuredImageWidth: 190
  },
  {
    featuredName: 'Intimates',
    featuredImage: featured_14,
    featuredOrientationImage: 'right',
    featuredImageWidth: 150
  },
  {
    featuredName: 'Sale',
    featuredImage: featured_16,
    featuredOrientationImage: 'right',
    featuredImageWidth: 200
  },
  {
    featuredName: 'Animale oro',
    featuredImage: featured_18
  },
  {
    featuredImage: featured_17,
    featuredContentHeight: 200
  }
]

const Featured: React.FC = () => {
  const navigation = useNavigation()
  return (
    <WrapperFeatured>
      {dataList.map((item, index) => {
        return (
          <ContentFeatured
            onPress={() => navigation.navigate('CategoryItems')}
            key={`featured_${index}`}
            backgroudColor="#c1c3c2"
            contentHeight={item?.featuredContentHeight}
          >
            <FeaturedBoxText orientation={item?.featuredOrientationImage}>
              <Text text={item?.featuredName} textColor="#fff" textSize={18} />
            </FeaturedBoxText>
            <FeaturedImage
              orientation={item?.featuredOrientationImage}
              imageWidth={item?.featuredImageWidth}
              source={item?.featuredImage}
            />
          </ContentFeatured>
        )
      })}
    </WrapperFeatured>
  )
}

export default Featured
