import React from 'react'
import { IButton } from '../../@types/IButton'
import { IText } from '../../@types/IText'
import { IActionBox } from '../../@types/IActionBox'
import { StyleActionBox } from './styles'

import Button from '../button'
import Text from '../text'

type Props = IActionBox & IButton & IText

const ActionBox: React.FC<Props> = props => {
  return (
    <StyleActionBox {...props} source={props.backgroundImage}>
      <Text
        textSize={props.titleSize}
        text={props?.title}
        textColor="#fff"
        textMargin={props.titleMargin}
      />
      <Button {...props} />
    </StyleActionBox>
  )
}

export default ActionBox
