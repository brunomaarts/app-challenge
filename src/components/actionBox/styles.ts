import styled from 'styled-components'
import { IActionBox } from '../../@types/IActionBox'

export const StyleActionBox = styled.ImageBackground<IActionBox>`
  width: ${props => props.boxWidth || '100%'};
  height: ${props => props.boxHeight || 'auto'};
  max-width: ${props => props.boxWidth || '100%'};
  max-height: ${props => props.boxHeight || 'auto'};
  background-color: ${props => props.backgroundColor || '#000'};
  display: flex;
  align-items: center;
  justify-content: center;
  padding: 15px 40px;
  flex-direction: column;
  margin: ${props => `${props.boxMargin || '0px'}`};
`
