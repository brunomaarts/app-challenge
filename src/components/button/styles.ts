import styled from 'styled-components'

export interface IStyleButton {
  btnBgColor: string
  btnType: 'filled' | 'leaked'
}

export const StyledButton = styled.TouchableOpacity<IStyleButton>`
  width: 100%;
  background-color: ${props =>
    props.btnType === 'leaked'
      ? 'transparent'
      : props.btnBgColor || 'transparent'};
  border: 2px solid ${props => props.btnBgColor || 'transparent'};
  padding: 10px 5px;
`
