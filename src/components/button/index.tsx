import React from 'react'
import { IButton } from '../../@types/IButton'
import { IText } from '../../@types/IText'
import { StyledButton } from './styles'
import Text from '../text'

type Props = IButton & IText

const Button: React.FC<Props> = props => {
  return (
    <StyledButton {...props} onPress={props.btnAction}>
      <Text {...props} />
    </StyledButton>
  )
}

export default Button
