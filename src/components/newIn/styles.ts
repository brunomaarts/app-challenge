import styled from 'styled-components'

export interface IStyleButton {
  boxHeight?: string
}

export const StyledNewInList = styled.FlatList`
  height: ${props => props.boxHeight || 'auto'};
  padding-left: 24px;
  flex-grow: 0;
`
