import React from 'react'
import { StyledNewInList } from './styles'

import ActionBox from '../actionBox'
import Model1 from '../../assets/images/modelo_1x.png'
import Model2 from '../../assets/images/modelo_2x.png'

const dataList = [
  {
    id: 'hot_trends',
    title: 'Hot Trends',
    titleButon: 'Shop Now',
    image: Model1,
    route: false
  },
  {
    id: 'jaquetas',
    title: 'Jaquetas',
    titleButon: 'Shop Now',
    image: Model2,
    route: false
  },
  {
    id: 'hot_trends_2',
    title: 'Hot Trends',
    titleButon: 'Shop Now',
    image: Model1,
    route: false
  }
]

const NewIn: React.FC = props => {
  return (
    <StyledNewInList
      horizontal={true}
      data={dataList}
      keyExtractor={(item, index) => 'newin_' + index}
      showsHorizontalScrollIndicator={false}
      renderItem={({ item, index }) => (
        <ActionBox
          backgroundImage={item.image}
          title={item.title}
          titleSize={20}
          titleMargin="15px 0px"
          btnAction={item.route}
          btnBgColor="#fff"
          btnType="leaked"
          boxMargin={`0px ${
            index + 1 === dataList.length ? '48' : '20'
          }px 0px 0px`}
          boxWidth="240px"
          boxHeight="320px"
          text={item.titleButon}
          textColor="#fff"
          textSize={18}
          textAlign="center"
        />
      )}
    />
  )
}

export default NewIn
