import styled from 'styled-components'

interface IStyledSection {
  internalSpace: boolean
}

export const StyledSection = styled.View`
  padding-bottom: 14px;
  padding-top: 24px;
`

export const HeaderSection = styled.View`
  padding-bottom: 24px;
  padding-top: 24px;
  padding-left: 24px;
  padding-right: 24px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`

export const ContentSection = styled.View<IStyledSection>`
  padding-left: ${props => (props.internalSpace ? '24px' : '0px')};
  padding-right: ${props => (props.internalSpace ? '24px' : '0px')};
`

export const SectionAction = styled.TouchableOpacity`
  display: flex;
`
