import React from 'react'
import {
  StyledSection,
  HeaderSection,
  ContentSection,
  SectionAction
} from './styles'

import { ISection } from '../../@types/ISection'
import Text from '../text'

const Section: React.FC<ISection> = props => {
  return (
    <StyledSection>
      {props.sectionTitle && (
        <HeaderSection>
          <Text text={props.sectionTitle} textSize={18} textColor="#000" />
          {props.actionText && (
            <SectionAction onPress={() => props?.action}>
              <Text
                text={props.actionText}
                textSize={18}
                texTransform="none"
                textFont="jost-light"
                textColor="#626262"
              />
            </SectionAction>
          )}
        </HeaderSection>
      )}
      <ContentSection internalSpace={props.internalSpace}>
        {props.children}
      </ContentSection>
    </StyledSection>
  )
}

export default Section
