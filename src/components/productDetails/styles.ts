import styled from 'styled-components'

interface IPoductDetails {
  boxMargin: string
  boxWidth: string
}

interface IPoductContentDetails {
  internalSpace: boolean
}

export const ProductImage = styled.Image`
  width: 100%;
  max-width: 100%;
  height: 350px;
  max-height: 350px;
  margin-bottom: 15px;
`

export const PoductDetails = styled.View<IPoductDetails>`
  width: ${props => `${props.boxWidth || '220px'}`};
  flex: 1 0 ${props => `${props.boxWidth || '220px'}`};
  max-width: ${props => `${props.boxWidth || '220px'}`};
  margin-right: ${props => `${props.boxMargin || '0px'}`};
  height: auto;
`

export const PoductContentDetails = styled.View<IPoductContentDetails>`
  padding: ${props => `${props.internalSpace ? '0px 15px 25px' : '0px'}`};
`

export const PoductContentText = styled.View`
  display: flex;
  flex-direction: row;
`
