import React from 'react'
import {
  PoductContentDetails,
  PoductContentText,
  PoductDetails,
  ProductImage
} from './styles'
import Text from '../../components/text'

interface IProductSale {
  oldValue: string
  newValue: string
}

interface IProductDetails {
  image: string
  text: string
  value: string
  installmentAmount: string
  sale?: IProductSale
  width?: string
  internalSpace?: boolean
  margin?: string
}

const ProductDetails: React.FC<IProductDetails> = props => {
  return (
    <PoductDetails boxMargin={props?.margin} boxWidth={props?.width}>
      <ProductImage source={props.image} />
      <PoductContentDetails internalSpace={props?.internalSpace}>
        <Text
          textFont="jost-light"
          texTransform="none"
          textColor="#000000"
          textSize={16}
          text={props.text}
        />
        <PoductContentText>
          <Text
            textFont="jost-bold"
            texTransform="none"
            textColor={props?.sale ? '#E02020' : '#000000'}
            textSize={16}
            textMargin="0px 10px 0px 0px"
            text={props?.sale ? props.sale.newValue : props.value}
          />
          {props?.sale && (
            <Text
              textFont="jost-medium"
              texTransform="none"
              textColor="#969696"
              textDecoration="line-through"
              textSize={16}
              text={props.sale.oldValue}
            />
          )}
        </PoductContentText>
        <Text
          textFont="jost-light"
          texTransform="none"
          textColor="#000000"
          textSize={15}
          text={props.installmentAmount}
        />
      </PoductContentDetails>
    </PoductDetails>
  )
}

export default ProductDetails
