export interface IText {
  text: string
  textFont?: string
  textColor: string
  textSize: number
  textAlign?: 'left' | 'center' | 'right'
  texTransform?: string
  textMargin?: string
  textDecoration?: 'line-through'
}
