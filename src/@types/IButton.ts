export interface IButton {
  btnBgColor: string
  btnType: 'filled' | 'leaked'
  btnAction: () => void
}
