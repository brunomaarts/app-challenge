export interface IActionBox {
  backgroundColor?: string
  backgroundImage?: string
  title?: string
  titleSize?: number
  titleMargin?: string
  boxMargin?: string
  boxWidth?: string
  boxHeight?: string
}
