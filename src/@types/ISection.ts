export interface ISection {
  sectionTitle?: string
  internalSpace?: boolean
  actionText?: string
  action?: () => void
}
