export interface IHeaderPage {
  pageTitle: string
  backAction?: () => void
}
