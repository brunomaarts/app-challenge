import React from 'react'
import { useFonts } from 'expo-font'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'

import AppLoading from 'expo-app-loading'
import Routes from './src/routes'

import CategoryItems from './src/pages/CategoryItems'

const App: React.FC = () => {
  const AppStack = createStackNavigator()

  let [fontsLoaded] = useFonts({
    'jost-bold': require('./src/assets/fonts/Jost-Bold.ttf'),
    'jost-medium': require('./src/assets/fonts/Jost-Medium.ttf'),
    'jost-light': require('./src/assets/fonts/Jost-Light.ttf'),
    'merriweather-bold': require('./src/assets/fonts/Merriweather-Bold.ttf'),
    'merriweather-regular': require('./src/assets/fonts/Merriweather-Regular.ttf'),
    'merriweather-light': require('./src/assets/fonts/Merriweather-Light.ttf')
  })

  if (!fontsLoaded) {
    return <AppLoading />
  }

  return (
    <NavigationContainer>
      <AppStack.Navigator screenOptions={{ headerShown: false }}>
        <AppStack.Screen name="Home" component={Routes} />
        <AppStack.Screen name="CategoryItems" component={CategoryItems} />
      </AppStack.Navigator>
    </NavigationContainer>
  )
}

export default App
